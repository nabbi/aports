# Contributor: Díaz Urbaneja Diego <sodomon2@gmail.com>
# Maintainer: Díaz Urbaneja Diego <sodomon2@gmail.com>
pkgname=ppsspp
pkgver=1.15.3
pkgrel=0
pkgdesc="PPSSPP - a fast and portable PSP emulator"
url="https://www.ppsspp.org/"
arch="aarch64 x86 x86_64 ppc64le" # other arches fail to build
license="GPL-2.0-only"
makedepends="
	cmake
	ffmpeg4-dev
	glew-dev
	libzip-dev
	mesa-dev
	miniupnpc-dev
	python3
	qt5-qtbase-dev
	qt5-qtmultimedia-dev
	samurai
	sdl2-dev
	snappy-dev
	zlib-dev
	zstd-dev
	"
source="https://github.com/hrydgard/ppsspp/releases/download/v$pkgver/ppsspp-$pkgver.tar.xz
	gcc13.patch
	"
options="!check" # make check not implemented

build() {
	cmake -B build-qt -G Ninja \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_BUILD_TYPE=Release \
		-DUSE_DISCORD=OFF \
		-DUSE_SYSTEM_FFMPEG=ON \
		-DUSE_SYSTEM_LIBPNG=ON \
		-DUSE_SYSTEM_LIBSDL2=ON \
		-DUSE_SYSTEM_LIBZIP=ON \
		-DUSE_SYSTEM_MINIUPNPC=ON \
		-DUSE_SYSTEM_SNAPPY=ON \
		-DUSE_SYSTEM_ZSTD=ON \
		-DUSING_QT_UI=ON \
		-DUSING_GLES2=ON \
		-DUSING_EGL=ON
	cmake --build build-qt
}

package() {
	DESTDIR="$pkgdir" cmake --install build-qt
}

sha512sums="
fee79e5410691262798f9b3c3bc940135bc4fa5ad005602ea44898f4047a446ad6e232a7ba78ba2a771f34ef1a5ced429e46eed318d2573e461443b9f2686154  ppsspp-1.15.3.tar.xz
febb3fa9c7c1d81178d1f7ca81ad2e6040f115369030ce9fe0bcc4f1a94e0ca2e875f3ee7c766f75b687a33c11d41bb4b8f4b97b08187a05337e9864656967ea  gcc13.patch
"

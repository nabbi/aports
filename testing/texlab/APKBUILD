# Maintainer: Dominika Liberda <ja@sdomi.pl>
# Contributor: Dominika Liberda <ja@sdomi.pl>
pkgname=texlab
pkgver=5.5.1
pkgrel=0
pkgdesc="An implementation of the Language Server Protocol for LaTeX"
url="https://github.com/latex-lsp/texlab"
# limited by rust/cargo
# armhf - fails to build
arch="x86_64 armv7 aarch64 x86 ppc64le"
license="GPL-3.0-or-later"
makedepends="cargo"
source="https://github.com/latex-lsp/texlab/archive/refs/tags/v$pkgver/texlab-v$pkgver.tar.gz"

# tests OOM on 32-bit
# x86_64/ppc64le tests broken with some things in /tmp
case "$CARCH" in
	x86|x86_64|ppc64le|armv7) options="!check" ;;
esac

export CARGO_PROFILE_RELEASE_PANIC="unwind"

prepare() {
	default_prepare
	cargo fetch --target="$CTARGET" --locked
}

build() {
	cargo build --release --frozen
}

check() {
	cargo test --frozen
}

package() {
	install -Dm755 target/release/texlab -t "$pkgdir"/usr/bin/
}

sha512sums="
6cc46cbb4f3b71df34174a055daa99d7a52e8a2b5ac47a3b20a5a1e2d693d70d35de66c754b55755ea23b52c57bc8c9a3cc7743fd33fe28e87a44177e221c89f  texlab-v5.5.1.tar.gz
"

# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kholidays
pkgver=5.106.0
pkgrel=0
pkgdesc="Support for icon themes"
arch="all !armhf" # armhf blocked by qt5-qtdeclarative
url="https://community.kde.org/Frameworks"
license="LGPL-2.0-or-later"
depends_dev="
	qt5-qtbase-dev
	qt5-qtdeclarative-dev
	"
makedepends="$depends_dev
	doxygen
	extra-cmake-modules
	qt5-qttools-dev
	samurai
	"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kholidays-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
69694ce79aac6c949bdc8968fecce862afc2fec5e57bcc80d0d8046ad30a2e4374ac275d2c94a2737715544d1f4fbfb1317342b7e45075ef8d659e4379fb24e0  kholidays-5.106.0.tar.xz
"

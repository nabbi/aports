# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kpeople
pkgver=5.106.0
pkgrel=0
pkgdesc="A library that provides access to all contacts and the people who hold them"
arch="all !armhf" # armhf blocked by qt5-qtdeclarative
url="https://community.kde.org/Frameworks"
license="LGPL-2.1-or-later"
depends="qt5-qtbase-sqlite"
depends_dev="qt5-qtdeclarative-dev kcoreaddons-dev kwidgetsaddons-dev ki18n-dev kitemviews-dev kservice-dev"
makedepends="$depends_dev extra-cmake-modules qt5-qttools-dev doxygen samurai"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kpeople-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	cd build
	# personsmodeltest fails
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest -E '(personsmodeltest)'
}

package() {
	DESTDIR="$pkgdir" cmake --build build --target install
}

sha512sums="
29164d13a23462853b2b1ed1b44ead9b3ae771a14fb34c018f8f62cd82cbdc495859457d21551d0125623bf0673f26085e100e34f46e32b630905dce7f6f81e3  kpeople-5.106.0.tar.xz
"

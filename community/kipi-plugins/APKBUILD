# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kipi-plugins
pkgver=23.04.1
pkgrel=0
pkgdesc="A collection of plugins extending the KDE graphics and image applications"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://userbase.kde.org/KIPI"
license="GPL-2.0-or-later AND GFDL-1.2-only"
makedepends="
	extra-cmake-modules
	karchive-dev
	kconfig-dev
	ki18n-dev
	kio-dev
	kwindowsystem-dev
	kxmlgui-dev
	libkipi-dev
	qt5-qtbase-dev
	qt5-qtsvg-dev
	qt5-qtxmlpatterns-dev
	samurai
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/kipi-plugins-$pkgver.tar.xz"
subpackages="$pkgname-lang"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
38f8c7027e39257ccf0866da82ca89e7b61654d21461c2ebe67e00453577374f290a11dcff862733545a08e762b0e2b9abb683110eddb4be097a8315ba3c3936  kipi-plugins-23.04.1.tar.xz
"

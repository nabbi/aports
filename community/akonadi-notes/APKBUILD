# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=akonadi-notes
pkgver=23.04.1
pkgrel=0
pkgdesc="Libraries and daemons to implement management of notes"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://community.kde.org/KDE_PIM"
license="LGPL-2.0-or-later"
depends_dev="
	ki18n-dev
	kmime-dev
	qt5-qtbase-dev
	"
makedepends="$depends_dev
	extra-cmake-modules
	samurai
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/akonadi-notes-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
af08593870a4619b13568533a655bbff85308aac0ff44bdbe85882d8a49250cf2286a6fddd1cef05824abee4e49ad08f97d35f438d2b764f7a7bfd8c2ffd19e0  akonadi-notes-23.04.1.tar.xz
"

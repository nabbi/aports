# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kmime
pkgver=23.04.1
pkgrel=0
pkgdesc="Library for handling mail messages and newsgroup articles"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://community.kde.org/KDE_PIM"
license="LGPL-2.0-or-later"
depends_dev="
	kcodecs-dev
	ki18n-dev
	qt5-qtbase-dev
	"
makedepends="$depends_dev
	extra-cmake-modules
	samurai
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/kmime-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build

	# kmime-headertest and kmime-messagetest are broken
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest -E "kmime-(header|message)test"
}

package() {
	DESTDIR="$pkgdir" cmake --build build --target install
}

sha512sums="
e4be404c2c9c5c856cc2a369031128e67d87aeb5051ab8b4b636ae29347e9f9a19a3bc547d4f1efad92bd83a213a9195581ffcf2408d770bed1cc20350f652ac  kmime-23.04.1.tar.xz
"

# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kjobwidgets
pkgver=5.106.0
pkgrel=0
pkgdesc="Widgets for tracking KJob instances"
arch="all !armhf" # armhf blocked by extra-cmake-modules
url="https://community.kde.org/Frameworks"
license="LGPL-2.1-only AND (LGPL-2.1-only OR LGPL-3.0-only)"
depends_dev="
	kcoreaddons-dev
	kwidgetsaddons-dev
	qt5-qtx11extras-dev
	"
makedepends="$depends_dev
	doxygen
	extra-cmake-modules
	graphviz
	qt5-qttools-dev
	samurai
	"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kjobwidgets-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
bc5796875b5184ed782110f1fc2ed375283763cec4b10afd581406a47d9a08aa803c05fcb6a9d64dd8fd30c7e718f823dc773f039c7309d6a5683644c2efb70f  kjobwidgets-5.106.0.tar.xz
"
